let bChangedFlag=false;
let nKeyLength=0;
//Onchange event of type dropdown
let test = function(e) {
  if(document.getElementsByName("keys").length==0){
    // append keys as checkboxes to the form
    fLoadPatientCheckboxes(e);
  }
  else{
    //empty the pre-existing keys's div and then append keys as checkboxes to the form
    console.log(e);
    if(document.getElementsByName("keys").length>0)
    fEmptyDiv('keys');
    fLoadPatientCheckboxes(e);
  }
  bChangedFlag=true;
}

function fEmptyDiv(mainDiv){
 let oMainDiv = document.getElementById(mainDiv);
  while (oMainDiv.firstChild) {
      oMainDiv.removeChild(oMainDiv.firstChild);
  }
}

/*
Function to create dynamic checkboxes and append it to the form
*/
function fLoadPatientCheckboxes(type) {
  let aKeyNames=[];
  for(let i=0;i<document.getElementsByName("keys").length;i++)
    aKeyNames.push(document.getElementsByName("keys")[i].value);
   
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      let arr=JSON.parse(this.response);
      let keys=arr.data.patient;
      nKeyLength=keys.length;
      for (var i = 0; i < nKeyLength; i++) {
        if(aKeyNames.includes(keys[i])){}
        else{
          let oPatientKeysCheckboxLabel=document.createElement("label");
          oPatientKeysCheckboxLabel.setAttribute( 'class','form-check-label');
          let oKey=fCreateCheckbox('keys',keys[i]);
          oKey.setAttribute( 'class','form-check-input');
          oPatientKeysCheckboxLabel.appendChild(oKey);
          oPatientKeysCheckboxLabel.appendChild(document.createTextNode(keys[i]));
          let oSpan=document.createElement('span');
          oSpan.setAttribute('class','form-check-sign');
          let oInnerSpan=document.createElement('span');
          oInnerSpan.setAttribute('class','check');
          oSpan.appendChild(oInnerSpan);
          oPatientKeysCheckboxLabel.appendChild(oSpan);
          let oInnerDiv=document.createElement('div');
          oInnerDiv.setAttribute('class','form-check form-check-inline');
          oInnerDiv.appendChild(oPatientKeysCheckboxLabel);
          document.getElementById("keys").appendChild(oInnerDiv);
        }
      }
    }
  };
  xhttp.open("GET", "/dynamic_report_generation_form_res/"+type, true);
  xhttp.send();
}
function fSetKeys(keys){
  let all_keys=document.getElementById('all_keys');
  all_keys.setAttribute('value',keys);
}
let oButton=document.getElementById('btnSubmit');
oButton.setAttribute('onclick','fSubmit()');

function fSubmit(){
  let aKeyNames=[];
  for(let i=0;i<document.getElementsByName("keys").length;i++)
    aKeyNames.push(document.getElementsByName("keys")[i].value);

  fSetKeys(aKeyNames);
  let oForm=document.getElementById('oUpdateForm');

  oForm.submit();
}
/*
Function to create a checkbox
*/
function fCreateCheckbox(sName, sValue, sIsChecked=null){
 let oCheckboxElement=document.createElement('input');
 oCheckboxElement.setAttribute('type','checkbox');
 oCheckboxElement.setAttribute('name',sName);
 oCheckboxElement.setAttribute('value',sValue);
 if(sIsChecked){
 oCheckboxElement.setAttribute('checked',1);    
 }
 return oCheckboxElement;
}

// function returnResult(callback)
//   {var mysql      = require('mysql');
//     var connection = mysql.createConnection({
//       host     : 'localhost',
//       user     : 'root',
//       password : 'plus91',
//       database : 'app_medixcel_americares'
//     });
//     connection.connect();
//     connection.query('select patient_json from app_medixcel_patient_data limit 1', function (error, results, fields) {
//       if (error)
//               callback(error,null);
//           else
//              {
//               let some1 = JSON.parse(JSON.stringify(results));
//       let keys=Object.keys(JSON.parse(some1[0].patient_json));
//       console.log(keys);
//       callback(null,keys);
//              } 
//       });
//     connection.end();

//   }