const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const fs=require('fs');
const mysql = require('mysql');
const multer  = require('multer')
const path    = require('path');
const defaultContentLength=500000;

const app = express();

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.set('view engine', 'ejs')
app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));

let oStaticData={sProfilePicFilePath:"uploads/profile_pics/plus91-profile-photo-1558699915103.jpg", nUserTypeId:2};//Compulsary to render with every page request
// Index page
app.get('/index', (req, res)=> {
	if(req.session.loggedin){
		oStaticData.sProfilePicFilePath=req.session.file_path;
		oStaticData.nUserTypeId=req.session.user_type_id;
		res.render('index',{static_data: oStaticData});	
	}
	else
	res.redirect('/');
});

// Dynamic report creation form
app.get('/dynamic_report_generation_form', (req, res)=> {
	res.render('dynamic_report_generation_form',{static_data: oStaticData});
});

// Dynamic report creation form request data collection and store response in the json file
app.post('/dynamic_report_generation_form', (req, res) =>{
	let aResponse=[];
	aResponse['keys']=[];
	aResponse['all_keys']=[];
	if(req.body.report_name!=undefined)
	aResponse['report_name']=req.body.report_name;
	if(req.body.type!=undefined)
	aResponse['type']=req.body.type;
	if(req.body.keys!=undefined)
	aResponse['keys']=req.body.keys;
	if(req.body.all_keys!=undefined)
	aResponse['all_keys']=req.body.all_keys.split(',');
	aResponse['report_record_limit']=req.body.report_record_limit;
	let sKeys=[];
	for(let key of aResponse['keys']){
		if(typeof(aResponse['keys'])=="string")
		{ 
			sKeys.push(`"${aResponse['keys']}"`); 
			break; 
		}
		else
			sKeys.push(`"${key}"`);
	}

	//Query object creation 
	let sQueryoJsonect=`{
		"db_name":"app_medixcel_americares",
		"table_name":"app_medixcel_patient_data",
		"statement":"select",
		"col_names":
		 	[
		 		{
		 			"table_name":"app_medixcel_patient_data", 
		 			"column":{
						"col_datatype":"json",
						"col_name":"patient_json",
						"key_names":[${sKeys}]
					}
		 		}
		 	],
		"function":"",
		"join":"",
		"join_info":[],
		"where":{
			"operator":"",
			"where_condition":[]
		},
		"order_by":"",
	  	"limit":[${aResponse['report_record_limit']}],
		  	"offset":""
	}`;

	let oQueryoJsonect=JSON.parse(sQueryoJsonect);
	let oJson = [];
	//If file exists and if data is present in file then append file or if data is not present then push report object in json file
	fs.exists('report_query.json', exists=>{
		if(exists){
			fs.readFile('report_query.json', 'utf8', function (err, data){
				if (err){
						console.log(err);
				} 
				else {
					oJson = JSON.parse(data); //now it an oJsonect
					let oReport={
						report_id: oJson.length +1,
						report_name: aResponse['report_name'],
						type : aResponse['type'],
						keys:aResponse['keys'],
						all_keys:aResponse['all_keys'],
						report_record_limit:aResponse['report_record_limit'],
						query : oQueryoJsonect
					};
					oJson.push(oReport); //add some data
					sJson = JSON.stringify(oJson); //convert it back to json
					fs.writeFile('report_query.json', sJson, 'utf8', function(err,contents){
						console.log("oJsonect Inserted..");
					}); // write it back 
				}
			});
		} 
		else {
			let oReport={
				report_id: 1,
				report_name: aResponse['report_name'],
				type : aResponse['type'],
				keys:aResponse['keys'],
				all_keys:aResponse['all_keys'],
				report_record_limit:aResponse['report_record_limit'],
				query : oQueryoJsonect
			};
		
			oJson.push(oReport);
			var sJson = JSON.stringify(oJson);
			fs.writeFile('report_query.json', sJson, 'utf8', function(err,contents){
				console.log("oJsonect Inserted while creating file");
			});
		}
	});
	aFileData=fs.readFileSync("report_query.json","utf8");
	oParsedData=JSON.parse(aFileData);
	res.render('dynamic_report_generation_form',{data:oParsedData,static_data: oStaticData});
});

// get the report id and send the data to the UI and autofill the data		
app.get('/update_report',(req,res)=>{
	// console.log('in get update report');
	let url_string="http://localhost:3000"+res.req._parsedOriginalUrl.path;
	let url = new URL(url_string);
	let report_id = url.searchParams.get("report_id");

	let sData;
	aFileData=fs.readFileSync("report_query.json","utf8");
	oParsedData=JSON.parse(aFileData);
	let sResult=null;
	let bFoundResult=false;
	for(let oDoc of oParsedData){
		if(parseInt(oDoc.report_id)==report_id){
			res.render('update_report',{data:oDoc,static_data: oStaticData});
			bFoundResult=true;
			break;
		}
	}
	if(!bFoundResult){
		res.render('update_report',{data:[],static_data: oStaticData});
	}
});

// / get the data amd save it in the database
app.post('/update_report',(req,res)=>{	
	let aResponse=[];
	aResponse['keys']=[];
	aResponse['all_keys']=[];

	if(req.body.report_id!=undefined)
	aResponse['report_id']=req.body.report_id;
	if(req.body.report_name!=undefined)
	aResponse['report_name']=req.body.report_name;
	if(req.body.type!=undefined)
	aResponse['type']=req.body.type;
	if(req.body.keys!=undefined)
	aResponse['keys']=req.body.keys;
	if(req.body.all_keys!=undefined)
	aResponse['all_keys']=req.body.all_keys.split(',');
	if(req.body.report_record_limit!=undefined)
	aResponse['report_record_limit']=req.body.report_record_limit;
	let sKeys=[];
	for(let key of aResponse['keys']){
		if(typeof(aResponse['keys'])=="string")
		{ 
			sKeys.push(`"${aResponse['keys']}"`); 
			break; 
		}
		else
			sKeys.push(`"${key}"`);
	}

	// Query object creation 
	let sQueryoJsonect=`{
		"db_name":"app_medixcel_americares",
		"table_name":"app_medixcel_patient_data",
		"statement":"select",
		"col_names":
		 	[
		 		{
		 			"table_name":"app_medixcel_patient_data", 
		 			"column":{
						"col_datatype":"json",
						"col_name":"patient_json",
						"key_names":[${sKeys}]
					}
		 		}
		 	],
		"function":"",
		"join":"",
		"join_info":[],
		"where":{
			"operator":"",
			"where_condition":[]
		},
		"order_by":"",
	  	"limit":[${aResponse['report_record_limit']}],
		  	"offset":""
	}`;

	let oQueryoJsonect=JSON.parse(sQueryoJsonect);
	// console.log(sQueryoJsonect);
	let oJson = [];
	//If file exists and if data is present in file then append file or if data is not present then push report object in json file
	fs.exists('report_query.json', exists=>{
		if(exists){
			fs.readFile('report_query.json', 'utf8', function (err, data){
				if (err){
						console.log(err);
				} 
				else {
					oJson = JSON.parse(data); //now it an oJsonect
					for(let i=0;i<oJson.length;i++){
						if(oJson[i].report_id==req.body.report_id)	
						oJson.splice(i, 1);
					}
					let oReport={
						report_id: aResponse['report_id'],
						report_name: aResponse['report_name'],
						type : aResponse['type'],
						keys:aResponse['keys'],
						all_keys:aResponse['all_keys'],
						report_record_limit:aResponse['report_record_limit'],
						query : oQueryoJsonect
					};

					oJson.push(oReport); //add some data

					sJson = JSON.stringify(oJson); //convert it back to json
					fs.writeFile('report_query.json', sJson, 'utf8', function(err,contents){
						if(err) console.log(err);
						console.log("oJsonect Updated..");
					}); // write it back 
				}
			});
		} 
		else {
			let oReport={
				report_id: 1,
				report_name: aResponse['report_name'],
				type : aResponse['type'],
				keys:aResponse['keys'],
				all_keys:aResponse['all_keys'],
				report_record_limit:aResponse['report_record_limit'],
				query : oQueryoJsonect
			};
		
			oJson.push(oReport);
			var sJson = JSON.stringify(oJson);
			fs.writeFile('report_query.json', sJson, 'utf8', function(err,contents){
				console.log("oJsonect Inserted while creating file");
			});
		}
	});
	aFileData=fs.readFileSync("report_query.json","utf8");
	oParsedData=JSON.parse(aFileData);
	res.redirect('report_listing');
});

//To get the keys
app.get('/dynamic_report_generation_form_res/:type', (req, res)=> {
	let connection = mysql.createConnection({
	  host     : 'localhost',
	  user     : 'root',
	  password : 'plus91',
	  database : 'app_medixcel_lite'
	});
	connection.connect();

	let sType=req.params.type; 
	switch(sType){
		case 'patient':
			connection.query(`SELECT form_json->'$.inputs' AS inputdata FROM app_form_master LIMIT 1`, function (error, results, fields) {
				if (error) throw error;
				// let oResult = JSON.parse(JSON.stringify(results));
				// let aKeys=Object.keys(JSON.parse(oResult[0].patient_json));
				let aKeys=[];
				let oResult=JSON.parse(JSON.stringify(results));
				let oInputData=JSON.parse(oResult[0].inputdata);
				for(let i=0;i<Object.keys(oInputData).length;i++)
				aKeys.push(oInputData[i].id);
				res.send({data:{patient:aKeys}});
			});
			break;
		default:
			break;
	} 
	connection.end();
});

// List of reports to see report data
app.get('/report', (req, res)=> {
	aFileData=fs.readFileSync("report_query.json","utf8");
	oParsedData=JSON.parse(aFileData);
	res.render('report',{data:oParsedData,static_data: oStaticData} );
});

// List of reports for updation
app.get('/report_listing', (req, res)=> {
	aFileData=fs.readFileSync("report_query.json","utf8");
	oParsedData=JSON.parse(aFileData);
	res.render('report_listing',{data:oParsedData,static_data: oStaticData} );
});
// Report data
app.get('/report_data', (req, res)=> {
	let url_string="http://localhost:3000"+res.req._parsedOriginalUrl.path;
	let url = new URL(url_string);
	let report_id = url.searchParams.get("report_id");
	let report_name=url.searchParams.get("report_name");
	let sData;
	aFileData=fs.readFileSync("report_query.json","utf8");
	oParsedData=JSON.parse(aFileData);
	let sResult=null;
	let bFoundResult=false;
	for(let oDoc of oParsedData){
		if(parseInt(oDoc.report_id)==report_id){
			report(oDoc,function(response){
				sResult=JSON.parse(JSON.stringify(response));
				res.render('report_data',{data:{report_name:report_name,report_data:sResult},static_data:oStaticData});
			});
			bFoundResult=true;
			break;
		}
	}
	if(!bFoundResult){
		res.render('report_data',{data:[],static_data: oStaticData});
	}
});

app.get('/upload', (req, res)=> {
	if(req.session.loggedin){
		res.render('upload',{static_data: oStaticData});
	}
	else
		res.redirect('/');
});

//Function to upload image(Profile Picture)
app.post('/upload', (req, res)=> {
	if(req.params.error)
		alert(req.params.error);
	if(req.session.loggedin){
		let oProfilePic=[];
		oProfilePic['username']=req.session.username;
		oProfilePic['original_file_name'];
		oProfilePic['current_file_name'];
		oProfilePic['file_path']='./public/uploads/profile_pics/';
		oProfilePic['file_extension'];
		oProfilePic['file_size']=512;
		var storage =   multer.diskStorage({
		  destination: function (req, file, callback) {
		    callback(null, oProfilePic['file_path']);
		  },
		  filename: function (req, file, callback) {
		  	oProfilePic['original_file_name']=file.originalname;
			oProfilePic['file_extension']=path.extname(file.originalname);
			oProfilePic['current_file_name']='plus91-'+file.fieldname + '-' + Date.now() + path.extname(file.originalname);
			oProfilePic['file_path']='uploads/profile_pics/'+oProfilePic['current_file_name'];
			callback(null,oProfilePic['current_file_name']);
		  }
		});
		var upload = multer({
	      storage: storage,
	      fileFilter: function (req, file, callback) {
	      	var ext = path.extname(file.originalname);
	        if(ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
	            return callback(new Error('Only images are allowed'))
	        }
	        if(req.headers['content-length']>defaultContentLength) {
	          return callback(new Error('Only file size below 5mb are allowed'))
	        }
	        callback(null, true)
	      }
	    }).single('profile-photo');
	    upload(req,res,function(err) {

	        if(err) {
	        console.log(err);
	        let error='Error in uploading file';
	            return res.redirect('/upload?error='+error);
	        }

	      	const connection = mysql.createConnection({
			  host     : 'localhost',
			  user     : 'root',
			  password : 'plus91',
			  database : 'mxcel_lite'
			});
			connection.connect();
			let bResult=0;
			connection.query(`SELECT * from profile_pictures where username='${oProfilePic['username']}';`, function (error, results, fields) {
				if (error) throw error;
				bResult=results;
				// console.log(bResult); 
				fChangeStatus();		       
			});
			function fChangeStatus(){
				// console.log(connection);
				aExistingPics=JSON.parse(JSON.stringify(bResult));
				for(let i=0;i<aExistingPics.length;i++){
					if(aExistingPics[i].status==1){
						let sQuery=`UPDATE profile_pictures SET status = 0 WHERE current_file_name='${aExistingPics[i].current_file_name}';`;
						connection.query(sQuery, function (error, results, fields) {
				  			if (error) throw error;
				  			console.log(results);
						});
					}
				}
			}
			req.session.file_path=oProfilePic['file_path'];
			oStaticData.sProfilePicFilePath=oProfilePic['file_path'];
			let sQuery=`INSERT INTO profile_pictures VALUES('${oProfilePic['username']}','${oProfilePic['original_file_name']}','${oProfilePic['current_file_name']}','${oProfilePic['file_path']}','${oProfilePic['file_extension']}','${oProfilePic['file_size']}','1')`;
			connection.query(sQuery, function (error, results, fields) {
				if (error) throw error;
				console.log(results);
			});

			/*
				Solution to 'Cannot enqueue Query after invoking quit' error
				Link: https://stackoverflow.com/questions/14333272/node-mysql-cannot-enqueue-a-query-after-calling-quit
			*/ 
			function exitHandler(options, err) {
			    connection.end();
			    if (options.cleanup)
			        console.log('clean');
			    if (err)
			        console.log(err.stack);
			    if (options.exit)
			        process.exit();
			}
			process.on('exit', exitHandler.bind(null, {cleanup: true}));

	        res.render('index',{static_data: oStaticData});
	    });
	}
	else
		res.redirect('/');
});
// To define port number to work on
const port=process.env.port||3000;
app.listen(port,()=>console.log(`Listening to port ${port}`));

// Function to create dynamic query
function report(oReport, callback){
	let oMysql=require('mysql');
	let connection;
	let oResult;		
	let sTableName;
	let saColNames;
	let aValues;
	let sQuery;
	let aWhereCondition;
	let sWhereConditions;
	let sWhere;
	let oQuery=oReport.query;
	let sStatement=oQuery.statement;
	switch(sStatement){

		case "insert":
			sDbName=oQuery.db_name;
			connection=oMysql.createConnection({
				host: "localhost",
				user: "root",
				password: "plus91",	
				database: sDbName
			});
			sTableName=oQuery.table_name;
			saColNames=oQuery.col_names;
			aValues=oQuery.values;
			let sVal="";
			for (let i = 0; i < aValues.length; i++) {
				if(i==aValues.length-1){
					if(aValues[i]==null){
						sVal+="null";
						continue;
					}
					sVal+=`"${aValues[i]}"`;	
				}
				else{
					if(aValues[i]==null){
						sVal+="null,";
						continue;
					}
					sVal+=`"${aValues[i]}",`;	
				}
			}
			sQuery=`INSERT INTO ${sTableName}(${saColNames}) VALUES(${sVal});`;
			connection.query(sQuery,function(error, results, fields){
			if(error) console.log(error);
			else
				console.log(results);
			});
			connection.end();
		break;

		case "update":
			sDbName=oQuery.db_name;
			connection=oMysql.createConnection({
				host: "localhost",
				user: "root",
				password: "plus91",	
				database: sDbName
			});
			sTableName=oQuery.table_name;
			saColNames=oQuery.col_names;
			aValues=oQuery.values;
			aWhereCondition=oQuery.where_condition;
			let sSet="";
			for (let i = 0; i < aValues.length; i++) {
				if(i==aValues.length-1){
					if(aValues[i]==null){
						sSet+=`${saColNames[i]}=null`;
						continue;
					}
					sSet+=`${saColNames[i]}="${aValues[i]}"`;	
				}
				else{
					if(aValues[i]==null){
						sSet+="${saColNames[i]}=null,";
						continue;
					}
					sSet+=`${saColNames[i]}="${aValues[i]}",`;	
				}
			}
			sWhereConditions=[];
			sWhere="";
			for (let i = 0; i < aWhereCondition.length; i++) {
				sWhere=`${aWhereCondition[i].col_name} ${aWhereCondition[i].condition} "${aWhereCondition[i].value}"`;
				sWhereConditions.push(sWhere);
			}
			
			sQuery=`UPDATE ${sTableName} SET ${sSet} WHERE (${sWhereConditions});`;

			connection.query(sQuery,function(error, results, fields){
			if(error) console.log(error);
			else
				console.log(results);
			});
			connection.end();
		break;

		case "delete":
			sDbName=oQuery.db_name;
			connection=oMysql.createConnection({
				host: "localhost",
				user: "root",
				password: "plus91",	
				database: sDbName
			});
			sTableName=oQuery.table_name;
			saColNames=oQuery.col_names;
			aValues=oQuery.values;
			aWhereCondition=oQuery.where_condition;
			
			sWhereConditions=[];
			sWhere="";
			for (let i = 0; i < aWhereCondition.length; i++) {
				sWhere=`${aWhereCondition[i].col_name} ${aWhereCondition[i].condition} "${aWhereCondition[i].value}"`;
				sWhereConditions.push(sWhere);
			}
			if(sWhereConditions.length==0)
				sQuery=`DELETE FROM ${sTableName};`;
			else
				sQuery=`DELETE FROM ${sTableName} WHERE ${sWhereConditions};`;

			connection.query(sQuery,function(error, results, fields){
			if(error) console.log(error);
			else
				console.log(results);
			});
			connection.end();
		break;

		case "select":
			sDbName=oQuery.db_name;
			connection=oMysql.createConnection({
				host: "localhost",
				user: "root",
				password: "plus91",	
				database: sDbName
			});
			sTableName=oQuery.table_name;
			oWhere=oQuery.where;
			sWhereOperator=oWhere.operator;
			aWhereCondition=oWhere.where_condition;
			let aoCol=oQuery.col_names;
			let sFunction=oQuery.function;
			let sJoin=oQuery.join;
			let aoJoinInfo=oQuery.join_info;
			let oOrderBy=oQuery.order_by;
			let nLimit=oQuery.limit;
			let nOffset=oQuery.offset;
			sQuery=select(sStatement, sTableName, sWhereOperator, aWhereCondition, aoCol, sFunction, sJoin, aoJoinInfo, oOrderBy, nLimit, nOffset);

			connection.query(sQuery,function(error, results, fields){
			if(error) console.log(error);
			else
			{
				oResult=results;
				// console.log(oResult);
				// fReturnResult();
				// return oResult;
				 return callback(oResult);
			}
			});
			connection.end();

		break;
	}
}
// function to create nested select query
function select(sStatement, sTableName, sWhereOperator, aWhereCondition, aoCol, sFunction, sJoin, aoJoinInfo, oOrderBy, nLimit, nOffset){
	let saCol;
	let sQuery;
	let sWhere;
	let sWhereConditions="";
	let oColumn;
	let sColDatatype;
	let sColName;
	let aKeyNameSeries;

	for (let i = 0; i < aWhereCondition.length; i++) {
		oColumn=aWhereCondition[i].column;
		sColDatatype=oColumn.col_datatype;
		sColName=oColumn.col_name;
		aKeyNameSeries=oColumn.key_name_series;
		sKeyNameSeries=(aKeyNameSeries.reduce((a,b)=>a+'.'+b));
		sWhere="";

		if(sWhereOperator=="NOT") sWhere=` NOT `;
		if(sColDatatype=="json")
			sWhere+=`(JSON_EXTRACT(${aWhereCondition[i].table_name}.${sColName},'$.${sKeyNameSeries}')) ${aWhereCondition[i].condition}`;
		else
			sWhere+=`${aWhereCondition[i].table_name}.${sColName} ${aWhereCondition[i].condition} ` ;

		if(aWhereCondition[i].condition=="IN"){
			if(aWhereCondition[i].value.type=="query"){
				let sSqStatement=aWhereCondition[i].value.values.statement;
				let sSqTableName=aWhereCondition[i].value.values.table_name;
				let sSqWhereOperator=aWhereCondition[i].value.values.where.operator;
				let aSqWhereCondition=aWhereCondition[i].value.values.where.where_condition;
				let aoSqColNames=aWhereCondition[i].value.values.col_names;
				let sSqFunction=aWhereCondition[i].value.values.function;
				let sSqJoin=aWhereCondition[i].value.values.join;
				let aoSqJoinInfo=aWhereCondition[i].value.values.join_info;
				let oSqOrderBy=aWhereCondition[i].value.values.order_by;
				let nSqLimit=aWhereCondition[i].value.values.limit;
				let nSqOffset=aWhereCondition[i].value.values.offset;
				let sSqNestedQuery=select(sSqStatement,sSqTableName, sSqWhereOperator, aSqWhereCondition, aoSqColNames, sSqFunction, sSqJoin, aoSqJoinInfo, oSqOrderBy, nSqLimit, nSqOffset);
				sWhere+=`( ${sSqNestedQuery}) `;
			}
			else sWhere+=`(${aWhereCondition[i].value.values})`;
		}
		else if(aWhereCondition[i].condition=="BETWEEN")
			sWhere+=`"${aWhereCondition[i].value.values[0]}" AND "${aWhereCondition[i].value.values[1]}" `;
		else
			sWhere+=`"${aWhereCondition[i].value.values}"`;
		sWhereConditions+=sWhere;
		if(i!=aWhereCondition.length-1 && sWhereOperator) sWhereConditions+=` ${sWhereOperator} `;
		else if(i!=aWhereCondition.length-1 && !sWhereOperator) sWhereConditions+=",";
	}
	saCol=[];
	for(let i=0;i<aoCol.length;i++){
		let sTableName=aoCol[i].table_name;
		sColDatatype=aoCol[i].column.col_datatype;
		sColName=aoCol[i].column.col_name;
		aKeyNames=aoCol[i].column.key_names;
		// console.log(sColDatatype);
		if(sColDatatype=="json"){
			for(let sKeyName of aKeyNames){
				saCol.push(`(${sTableName}.${sColName}->'$.${sKeyName}') AS ${sKeyName}`);
			}
		}
		else
			saCol.push(` ${sTableName}.${sColName}`);
	}
	sQuery=`${sStatement} `;
	if(sFunction){
		if(aoCol.length==1)
		sQuery+=` ${sFunction}($(saCol)) FROM ${sTableName} `;

	}
	else sQuery+=`${saCol} FROM ${sTableName} `;
	if(sJoin){
		if(aoJoinInfo.length!=0){
			for(let i=0;i<aoJoinInfo.length;i++){
				sQuery+=`${sJoin} JOIN ${aoJoinInfo[i].table2.table_name} ON ${aoJoinInfo[i].table1.table_name}.${aoJoinInfo[i].table1.col_name}=${aoJoinInfo[i].table2.table_name}.${aoJoinInfo[i].table2.col_name} `;
			}
		}
	}
	if(sWhereConditions.length!=0)
		sQuery+=`WHERE ${sWhereConditions} `;
	if(oOrderBy)
		sQuery+=`ORDER BY ${oOrderBy.table_name}.${oOrderBy.col_name} ${oOrderBy.asc_desc} `
	if(nLimit)
	{
		if(nLimit.length==1) {
			sQuery+=`LIMIT ${nLimit} `;
			if(nOffset)
				sQuery+=` OFFSET ${nOffset} `;
		}
		else if(nLimit.length==2){
			sQuery+=` LIMIT ${nLimit} `;
		}
	}	
	
	return sQuery;
}

app.get('/', function(req, res) {
	res.render('login');
});

app.post('/auth', function(request, response) {
	var connection = mysql.createConnection({
		host     : 'localhost',
		user     : 'root',
		password : 'plus91',
		database : 'nodelogin'
	});

	var username = request.body.username;
	var password = request.body.password;
	if (username && password) {
		connection.query('SELECT * FROM accounts WHERE username = ? AND password = ?', [username, password], function(error, results, fields) {

			if (results.length > 0) {
				request.session.loggedin = true;
				request.session.username = username;
				request.session.user_type_id=JSON.parse(JSON.stringify(results))[0].user_type_id;
				response.redirect('/home');
			} else {
				response.redirect('/');
			}			
			response.end();
		});
	} else {
		response.render('login');
		response.end();
	}
});

app.get('/home', function(request, response) {
	if (request.session.loggedin) {
		var connection = mysql.createConnection({
			host     : 'localhost',
			user     : 'root',
			password : 'plus91',
			database : 'mxcel_lite'
		});
		connection.query(`SELECT file_path FROM profile_pictures WHERE username = ? AND status='1'`, [request.session.username], function(error, results, fields) {
			if (results.length > 0) {
				request.session.file_path=JSON.parse(JSON.stringify(results))[0].file_path;
				
				// console.log(JSON.parse(JSON.stringify(results))[0].file_path);
				response.redirect('/index');
			}
		});
		// response.redirect('/index');
	} else {
		response.send('Please login to view this page!');
	response.end();
	}
});

app.get('/logout', function(request, response) {
	if(request.session.loggedin)
		request.session.loggedin=false;
	response.redirect('/');
});